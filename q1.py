#Python Program for an SQL Engine

#SELECT COLUMNS from TABLE;
def select(tablename, columns, func_array):
	#Set of all columns
	colu = tables[tablename]
	print '\n'
	print '\n'
	s = "%%%%%%%%%%%%%%%%%%%% " + tablename.upper() + " %%%%%%%%%%%%%%%%%%%%"
	print s

	eqm = []

	for cntr2 in range(0, len(columns)):
		for cntr3 in range(0, len(colu)):
			if columns[cntr2] == colu[cntr3]:
				eqm = eqm + [cntr3]


	print columns, func_array, colu, eqm

	print '\n'

	disp = []
	name = []
	cntr5 = 0

	for cntr in range(0,len(columns)):
		if func_array[cntr] == 'MAX':
			array = []
			x = "MAX(" + columns[cntr] + ")"
			name = name + [x]
			for row in range(0, len(values[tablename])):
				#for col in eqm:
				array = array + ([values[tablename][row][eqm[cntr]]])
			#print max(array)
			disp = disp + [[max(array)]]
			cntr5 += 1



		elif func_array[cntr] == 'MIN':

			array = []
			name = name + ["MIN(" + columns[cntr] + ")"]
			for row in range(0, len(values[tablename])):
				#for col in eqm:
				array = array + ([values[tablename][row][eqm[cntr]]])
			disp = disp + [[min(array)]]
			cntr5 += 1

		elif func_array[cntr] == 'AVG':
			array = []
			name = name + ["AVG(" + columns[cntr] + ")"]			
			for row in range(0, len(values[tablename])):
				#for col in eqm:
				array = array + ([values[tablename][row][eqm[cntr]]])
			disp = disp + [[reduce(lambda x, y: x + y, array) / len(array)]]
			cntr5 += 1 

		elif func_array[cntr] == 'SUM':
			array = []
			name = name +  ["SUM(" + columns[cntr] + ")"]
			for row in range(0, len(values[tablename])):
				#for col in eqm:
				array = array + ([values[tablename][row][eqm[cntr]]])
			disp = disp + [[sum(array)]]
			cntr5 += 1

		elif func_array[cntr] == 'DISTINCT':
			array = []
			name = name +  ["DISTINCT(" + columns[cntr] + ")"]
			for row in range(0, len(values[tablename])):
				#for col in eqm:
				if values[tablename][row][eqm[cntr]] not in array:
					array = array + ([values[tablename][row][eqm[cntr]]])
				else:
					array = array + ['-x-']
	
			disp = disp + [array]
			cntr5 += 1

		if func_array[cntr] == 'NONE':
			array = []
			
			name = name + [str(columns[cntr])]
			for row in range(0, len(values[tablename])):
				array = array + [values[tablename][row][eqm[cntr]]]
				#if col == eqm2[len(eqm)-1]:
				#print '\n'

			disp = disp + [array]
			cntr5 += 1

	l = len(values[tablename]) 
	for cntr in range(0, len(disp)):
		s = len(disp[cntr])
		for cntr2 in range(0, l-s):
			disp[cntr] += ['-x-']

	for cntr in range(0, len(name)):		
		print name[cntr],	
	print '\n'

	for cntr3 in range(0, len(disp[0])):
		for cntr in range(0, len(disp)):
			#if disp[cntr][cntr3] != '-x-':
			print disp[cntr][cntr3],
		print '\n'		 

def selectwhere(tablename1, tablename2, col_table1, col_table2, join_tables):
	colu1 = tables[tablename1]
	colu2 = tables[tablename2]
	
	eqm1 = []

	for cntr2 in range(0, len(col_table1)):
		for cntr3 in range(0, len(colu1)):
			if col_table1[cntr2] == colu1[cntr3]:
				eqm1 = eqm1 + [cntr3]

	eqm2 = []

	for cntr2 in range(0, len(col_table2)):
		for cntr3 in range(0, len(colu2)):
			if col_table2[cntr2] == colu2[cntr3]:
				eqm2 = eqm2 + [cntr3]

	cond1 = join_tables[0].split('.')
	if cond1[1] in tables[cond1[0]]:
		counter1 = 0
		for x in tables[cond1[0]]:
			if x == cond1[1]:
				break
			else:
				counter1+=1
	cond2 = join_tables[1].split('.')
	if cond2[1] in tables[cond2[0]]:
		counter2 = 0
		for x in tables[cond2[0]]:
			if x == cond2[1]:
				break
			else:
				counter2+=1
	#for cntr in range(0,len(col_table1)):
	name1 = []
	name2 = []
	for cntr1 in range(0,len(col_table1)):
		name1 = name1 + [str(col_table1[cntr1])]
	for cntr2 in range(0,len(col_table2)):
		name2 = name2 + [str(col_table2[cntr2])]
	disp1 = []
	disp2 = [] 
	for row in range(0, len(values[tablename1])):
		array1 = []
		array2 = []
		if values[cond1[0]][row][counter1] == values[cond2[0]][row][counter2]:
			for cntr in range(0,len(col_table1)):
				array1 = array1 + [values[tablename1][row][eqm1[cntr]]]
			disp1 = disp1 + [array1]

			for cntr0 in range(0,len(col_table2)):
				array2 = array2 + [values[tablename2][row][eqm2[cntr0]]]
			disp2 = disp2 + [array2]


	for cntr in range(0, len(name1)):		
		print name1[cntr],
	for cntr in range(0, len(name2)):		
		print name2[cntr],
	print '\n'

	for cntr3 in range(0, len(disp1)):
		for cntr in range(0, len(disp1[0])):
			print disp1[cntr3][cntr],

		for cntr in range(0, len(disp2[0])):
			print disp2[cntr3][cntr],

		print '\n'	


	#print col_table1, col_table2, colu1, colu2, eqm1, eqm2, cond1, cond2, counter1, counter2

def select_dist(tablename, columns):
	#Set of all columns
	colu = tables[tablename]
	print '\n'
	print '\n'
	s = "%%%%%%%%%%%%%%%%%%%% " + tablename.upper() + " %%%%%%%%%%%%%%%%%%%%"
	print s

	print columns

	array = []
	for x in columns:
		if 'distinct' in x:
			sp2 = x.split('(')
			sp3 = sp2[1].split(')')
			array = array + [sp3[0]]
		else: 
			array = array + [x]
	



import csv

f = open('metadata.txt', 'r')
tables = {}

#Loop that counts the no. of lines
size = 0
while True:
	line = f.readline()	
	size += 1
	if not line:
		break

#Setting file pointer to beg of file
f.seek(0, 0)

cntr2 = 0

#Loop to read metadata file
while cntr2 < size:
    line = f.readline()
    cntr2 += 1
    
    #Read from begin_table tag
    if line == '<begin_table>\r\n':
    	cntr = 0
    	while True:
    		metaline = f.readline()

    		#Counter for first entry after begin_table as table name
    		cntr += 1

    		#Counts total no. of lines in the file 
    		cntr2 += 1
    		
    		#Read till end table tag
    		if metaline == '<end_table>\r\n' or metaline == '<end_table>':
    			break
    		
    		if not metaline:
    			break	

    		if cntr == 1:
    			table_name = metaline[0:len(metaline)-2]
    			tables[table_name] = []
    		else:
    			tables[table_name] += [metaline[0:len(metaline)-2]]

f.close()

values = {}

#Read line-wise for each table
for i in tables.keys():
	#Name of table
	values[i] = []

	filename = i + '.csv'
	f = open(filename, 'r')
	
	while True:
		op = f.readline()
		if not op: 
		    break

		temp = op[0:len(op)-2].split(',')
		#print csv.reader(temp, quotechar='\'', delimiter=',')

		for cntr3 in range(0, len(temp)):
			temp[cntr3] = int(temp[cntr3])

		values[i] = values[i] + [temp]

	f.close()

command = str(raw_input('Enter the SQL Command - '))
parts = command.split(' ')
parts[0] = parts[0].upper()
parts[3] = parts[3].split(';')
parts[3] = parts[3][0].split(',')


#print parts

if parts[0] == 'SELECT':
	if parts[1] == '*':
		a = []
		for cntr3 in range(0, len(tables[parts[3][0]])):
			a = a + ['NONE']
		
		if len(parts) < 5:
			for i in range(0,len(parts[3])):
				select(parts[3][i], tables[parts[3][i]], a)
		else:
			parts[5] = parts[5].split(';')
			parts[5] = parts[5][0].split('=')
			selectwhere(parts[3][0],parts[3][1],tables[parts[3][0]],tables[parts[3][1]],parts[5])

	else:
		if len(parts) < 5:
			parts[1] = parts[1].split(',')
			col_array = []
			func_array = []
			table_map = []
			for x in parts[1]:
				x = x.upper()
				if 'DISTINCT' in x:
					sp2 = x.split('(')
					sp3 = sp2[1].split(')')
					col_array = col_array + [sp3[0].upper()]
					func_array = func_array + [sp2[0].upper()]
					print col_array,func_array, table_map
				elif 'MAX' in x:
					sp2 = x.split('(')
					sp3 = sp2[1].split(')')
					col_array = col_array + [sp3[0].upper()]
					func_array = func_array + [sp2[0].upper()]
				elif 'MIN' in x:
					sp2 = x.split('(')
					sp3 = sp2[1].split(')')
					col_array = col_array + [sp3[0].upper()]
					func_array = func_array + [sp2[0].upper()]
				elif 'AVG' in x:
					sp2 = x.split('(')
					sp3 = sp2[1].split(')')
					col_array = col_array + [sp3[0].upper()]
					func_array = func_array + [sp2[0].upper()]
				elif 'SUM' in x:
					sp2 = x.split('(')
					sp3 = sp2[1].split(')')
					col_array = col_array + [sp3[0].upper()]
					func_array = func_array + [sp2[0].upper()]
				else:
					col_array = col_array + [x.upper()]
					func_array = func_array + ['NONE']


			for cntr3 in range(0, len(parts[3])):
				select(parts[3][cntr3], col_array, func_array)

		else:
			parts[5] = parts[5].split(';')
			parts[5] = parts[5][0].split('=')
			col_array1 = []
			col_array2 = []
			parts[1] = parts[1].split(',')
			for x in parts[1]:
				if x in tables[parts[3][0]]:
					col_array1 = col_array1 + [x]
			for x in parts[1]:
				if x in tables[parts[3][1]]:
					col_array2 = col_array2 + [x]		
					
			selectwhere(parts[3][0],parts[3][1],col_array1,col_array2,parts[5])









